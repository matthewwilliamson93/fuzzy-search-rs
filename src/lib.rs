// use fst::automaton::Levenshtein;
// use fst::{IntoStreamer, Set, Streamer};

use ngrammatic::{Corpus, CorpusBuilder, Pad};

use pyo3::prelude::*;
use pyo3::types::PyList;

/// A Python class for an NGram
#[pyclass]
struct NGram {
    corpus: Corpus,
}

#[pymethods]
impl NGram {
    /// Returns a NGram object
    ///
    /// # Arguments
    ///
    /// * `items` - A list of items to put in the NGram
    #[new]
    fn new(items: &PyList) -> Self {
        let mut corpus = CorpusBuilder::new().arity(3).pad_full(Pad::Auto).finish();
        for item in items {
            corpus.add_text(&item.to_string());
        }

        NGram { corpus }
    }

    /// Returns a list of fuzzy matches along with their relevance
    ///
    /// # Arguments
    ///
    /// * `query` - The query string to search on.
    fn search(self_: PyRef<Self>, query: &str) -> Vec<(String, f32)> {
        self_
            .corpus
            .search(query, 0.95)
            .into_iter()
            .map(|result| (result.text, result.similarity))
            .collect()
    }
}

/// A Python class for a FST
#[pyclass]
struct Fst {}

/*
#[pymethods]
impl Fst {
    /// Returns a Fst object
    ///
    /// # Arguments
    ///
    /// * `items` - A list of items to put in the Fst
    #[new]
    fn new(items: &PyList) -> Self {
        Fst {
        }
    }

    /// Returns a list of fuzzy matches
    ///
    /// # Arguments
    ///
    /// * `query` - The query string to search on.
    fn search(self_: PyRef<Self>, query: &str) -> Vec<String> {
        let mut results: Vec<String> = vec![];
        results
    }
}
*/

/// A Python module implemented in Rust
#[pymodule]
fn fuzzy_search_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<NGram>()?;
    m.add_class::<Fst>()?;
    Ok(())
}
