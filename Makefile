PYTHON      ?= python3.10
TESTS        = tests
TESTS_FILES := $(shell find $(TESTS) -name "*.py")

.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:             Prints this screen"
	@echo "    venv:             Creates a virtual env"
	@echo "    install-dev-deps: Installs dev dependencies"
	@echo "    build:            Builds the Rust code into a pypi package"
	@echo "    install:          Installs the local pypi package into the env"
	@echo "    check-fmt:        Checks the code for style issues"
	@echo "    fmt:              Make the formatting changes directly to the project"
	@echo "    lint:             Lints the code"
	@echo "    test:             Runs the local tests"
	@echo "    clean:            Clean out temporaries"
	@echo ""

.PHONY: venv
venv:
	$(PYTHON) -m venv .venv

.PHONY: install-dev-deps
install-dev-deps:
	$(PYTHON) -m pip install --no-cache-dir -r requirements-dev.txt

.PHONY: build
build:
	maturin build --release

.PHONY: install
install: build
	$(PYTHON) -m pip install .

.PHONY: check-fmt
check-fmt:
	@echo "Format Checking"
	cargo fmt --all -- --check
	$(PYTHON) -m black --check $(TESTS)

.PHONY: fmt
fmt:
	@echo "Auto Formatting"
	cargo fmt --all
	$(PYTHON) -m black $(TESTS)

.PHONY: lint
lint:
	@echo "Rust linting"
	@cargo clippy
	@echo "Python Type Checking"
	@$(PYTHON) -m mypy --ignore-missing-imports $(TESTS_FILES)
	@echo "Python Linting"
	@$(PYTHON) -m pylint $(TESTS_FILES)

.PHONY: run
test:
	$(PYTHON) -m pytest -rP $(TESTS)

.PHONY:
clean:
	@echo "Removing temporary files"
	@rm -rf target/ "*.pyc" "$(TESTS)/__pycache__" ".mypy_cache" ".pytest_cache"
