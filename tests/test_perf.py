"""
A testing module to check performance of the rust_ngram.NGram class.
"""

import random
import time
from typing import Callable

from prettytable import PrettyTable
from ngram import NGram

from fuzzy_search_rs import NGram as RustNGram


with open("tests/words.txt") as file_handle:
    WORDS = [word.strip() for word in file_handle.readlines()]


def rust_create_ngram(items: list[str]) -> RustNGram:
    """
    A helper function to create a NGram with rust based module.

    Args:
        items: The list of items to build the NGram with.

    Returns:
        The NGram object.
    """
    return RustNGram(items=items)


def python_create_ngram(items: list[str]) -> NGram:
    """
    A helper function to create a NGram based on the pypi module.

    Args:
        items: The list of items to build the NGram with.

    Returns:
        The NGram object.
    """
    return NGram(items=items)


def rust_search_ngram(ngram: RustNGram, query: str) -> list[tuple[str, int]]:
    """
    A helper function to search on a NGram with rust based module.

    Args:
        ngram: The NGram to search on.
        query: The query string to search.

    Returns:
        A list of tuples of the result and its relevancy.
    """
    return ngram.search(query)


def python_search_ngram(ngram: NGram, query: str) -> list[tuple[str, int]]:
    """
    A helper function to search on a NGram based on the pypi module.

    Args:
        ngram: The NGram to search on.
        query: The query string to search.

    Returns:
        A list of tuples of the result and its relevancy.
    """

    return ngram.search(query, 0.95)


def bench_create(func: Callable, items: list[str]) -> float:
    """
    A function to benchmark the creation logic.

    Args:
        func: The function to benchmark.
        items: The list of items to build the NGram with.

    Returns:
        The amount of time it took for the test to run.
    """
    start = time.time()
    func(items)
    total = round(time.time() - start, 5)
    print(".", end="", flush=True)
    return total


def bench_search(func: Callable, klass, items: list[str], queries: list[str]) -> float:
    """
    A function to benchmark the creation logic.

    Args:
        func: The function to benchmark.
        klass: The class you will end up building.
        items: The list of items to build the NGram with.
        queries: A list of query strings to search.

    Returns:
        The amount of time it took for the test to run.
    """
    ngram = klass(items)
    start = time.time()
    for query in queries:
        func(ngram, query)
    total = round(time.time() - start, 5)
    print(".", end="", flush=True)
    return total


def test_perf_create() -> None:
    """
    Performance tests for create.
    """
    # GIVEN
    functions = [rust_create_ngram, python_create_ngram]
    sizes = [100, 1000, 10000]
    items_list = [WORDS[:n] for n in sizes]

    table = PrettyTable()
    table.field_names = ["Functions", "Small", "Medium", "Large"]
    table.align["Functions"] = "l"
    table.align["Small"] = "r"
    table.align["Medium"] = "r"
    table.align["Large"] = "r"

    # WHEN
    for func in functions:
        table.add_row(
            [
                func.__name__,
                *[bench_create(func, items) for items in items_list],
            ]
        )

    # THEN
    print("Benchmarking Create", end="\n", flush=True)
    print(table)


def test_perf_search() -> None:
    """
    Performance tests for search.
    """
    # GIVEN
    params = [
        (rust_search_ngram, RustNGram),
        (python_search_ngram, NGram),
    ]
    sizes = [100, 1000, 10000]
    items_list = [WORDS[:n] for n in sizes]
    queries_list = [random.sample(items, min(sizes)) for items in items_list]

    table = PrettyTable()
    table.field_names = ["Functions", "Small", "Medium", "Large"]
    table.align["Functions"] = "l"
    table.align["Small"] = "r"
    table.align["Medium"] = "r"
    table.align["Large"] = "r"

    # WHEN
    for func, klass in params:
        table.add_row(
            [
                func.__name__,
                *[
                    bench_search(func, klass, items, queries)
                    for items, queries in zip(items_list, queries_list)
                ],
            ]
        )

    # THEN
    print("Benchmarking Search", end="\n", flush=True)
    print(table)
